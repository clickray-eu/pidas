"use strict";

// file: events.js

// end file: events.js

// file: global.js
$(document).ready(function () {
    new WOW().init();
    setTimeout(function () {
        headerMenuFlyout();
    }, 500);
    headerSearchInit();
    menuMobileInit();
    displayVideoAsPopup("a.va-video-play-btn", "href");

    $('.print-page').click(function () {
        window.print();
    });
});
function headerMenuFlyout() {
    if ($("header.header--main .hs-menu-flow-horizontal > ul").length > 0) {

        $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top", -20);
        $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top", 77);

        $.each($("header.header--main .hs-menu-flow-horizontal > ul li.hs-menu-depth-2"), function (i, e) {
            $(e).parent().css("max-width", $(e).parents("ul:first").css("width"));
            if ($(e).find(">ul").length > 0) {
                //$(e).find(">ul").css("max-width",$(e).parent().css("width"));
                $.each($(e).find("li.hs-menu-depth-3"), function (j, el) {

                    $(el).parent().css("max-width", $(el).parent().css("width"));
                });
            }
        });
        $("header.header--main .hs-menu-flow-horizontal > ul > li").addClass("right");

        $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top", "");
        $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top", "");
        var tooBig = false;
        $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li").mouseover(function () {
            if (tooBig) {
                if (!$(this).hasClass("hover")) {
                    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li").removeClass("hover");
                    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top", "");
                }
                $(this).find("ul").offset({ top: $(this).parent().offset().top });
                $(this).addClass("hover");
            } else {

                $(this).find("ul").css("top", -20);
                if (!$(this).index(0)) {
                    $(this).find("ul").css("top", 0);
                }
                $(this).removeClass("hover");
            }
        });

        $(window).resize(function () {
            if (parseInt($(".header.header--main .menu").data("mobile-active")) >= window.innerWidth) {
                $(".header.header--main").addClass("mobile");
                return;
            } else {
                $(".header.header--main").removeClass("mobile");
            }
            $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top", -20);
            $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top", 77);
            $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("height", "auto");
            tooBig = false;
            $.each($("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul"), function (i, e) {
                if ($(e).offset().top + $(e).height() >= parseInt($(window).scrollTop()) + parseInt(window.innerHeight)) {
                    tooBig = true;
                }
            });
            $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top", "");
            $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top", "");
            $.each($("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li"), function (i, e) {
                $($(e).find("ul")).each(function (j, list) {
                    if ($(e).offset().left + parseInt($(e).css("width")) + parseInt($(list).css("width")) > window.innerWidth - 25) {
                        $(list).addClass("left").removeClass("right");
                        $(e).addClass("left-arrow").removeClass("right-arrow");
                    } else {
                        $(list).addClass("right").removeClass("left");
                        $(e).addClass("right-arrow").removeClass("left-arrow");
                    }
                    if (tooBig) {
                        if ($(list).parents("ul:first").height() > $(list).height()) $(list).css("height", $(list).parents("ul:first").css("height"));
                    } else {
                        $(list).css("height", "auto");
                        $(list).css("opacity", "");
                    }
                });
            });
        });

        $(window).resize();
    }
}
function menuMobileInit() {
    $('header.header--main .menu .hs-menu-wrapper>ul').slicknav({
        prependTo: 'header.header--main>.container>.span12>.row-fluid-wrapper:first-of-type>.row-fluid',
        label: "",
        duration: 500,
        init: function init() {
            $("header.header--main .slicknav_menu .slicknav_nav>li:first").before("<li class='search'></li>");
            $("header.header--main .slicknav_menu .slicknav_nav>li:last").after("<li class='follow-us'></li>");
            var search = $("header.header--main .google-search-module-popup .google-search-module-form").clone(true);
            $("header.header--main .slicknav_menu .slicknav_nav li.search").append(search);
            $("header.header--main .slicknav_menu .slicknav_nav li.follow-us").append($("header.header--main .follow-us-module").html());
        }
    });
    $('.header .slicknav_menu .slicknav_btn').on('click', function () {
        $('header.header--main .slicknav_menu .slicknav_nav').toggleClass('open');
    });
}
function headerSearchInit() {
    $("header.header--main .google-search-module-btn").click(function () {
        if (!$("header.header--main .google-search-module-popup").hasClass("show-popup")) $("header.header--main .google-search-module-popup").addClass("show-popup");else $("header.header--main .google-search-module-popup").removeClass("show-popup");
    });
    $("header.header--main .google-search-module-popup-close").click(function () {
        if ($("header.header--main .google-search-module-popup").hasClass("show-popup")) $("header.header--main .google-search-module-popup").removeClass("show-popup");
    });
    $("header.header--main .google-search-module-popup").click(function (e) {
        e.stopPropagation();
        if ($("header.header--main .google-search-module-popup").hasClass("show-popup")) $("header.header--main .google-search-module-popup").removeClass("show-popup");
    });
    $("header.header--main .google-search-module-form,header.header--main .google-search-module-icon,header.header--main .google-search-module-label").click(function (e) {
        e.stopPropagation();
    });
    $("header.header--main .google-search-module-icon").click(function () {
        if ($("header.header--main .google-search-module-popup form a").length > 0) $("header.header--main .google-search-module-popup form a").click();else $("header.header--main #google-custom-search").submit();
    });
}

function displayVideoAsPopup(selector, attr) {
    $(selector).not('.no-modal').each(function (i, e) {
        var videoSrc = $(e).attr(attr);
        if ($(e)[0].localName == "a") {
            $(e).attr("href", "javascript:void(0)");
        }
        $(e).magnificPopup({
            items: {
                type: 'inline',
                src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
            },
            callbacks: {
                open: function open() {
                    $('html').css('margin-right', 0);
                    $(this.content)[0].play();
                },
                close: function close() {
                    $(this.content)[0].load();
                }
            }
        });
    });
}

function findMiddleSlide(slideClass) {
    var slidesNum, initialSlide;
    slidesNum = $(slideClass).length;
    if (slidesNum > 2) {
        if (slidesNum % 2 == 0) {
            initialSlide = Math.floor(slidesNum / 2) - 1;
        } else {
            initialSlide = Math.floor(slidesNum / 2);
        }
    } else {
        initialSlide = 0;
    }
    return initialSlide;
}
// end file: global.js

// file: va-testimonial-video-slider.js
$(document).ready(function () {
    $('.va-testimonial-video-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
        accessability: false,
        swipe: false,
        draggeable: false,
        dots: true,
        infinite: false,
        dotsClass: 'va-testimonial-video-slider-pagination',
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: findMiddleSlide('.va-testimonial-video-slide'),
        vertical: true
    });

    facePaginationInit();
    showTestimonialVideo();
});

function facePaginationInit() {
    $('.va-testimonial-video-slider .slick-initialized .slick-slide').each(function () {
        var slideID = $(this).attr('aria-describedby');
        var avatarCompany = $(this).find('.hidden *');
        avatarCompany.appendTo($('.va-testimonial-video-slider-pagination li#' + slideID));
    });
}

function showTestimonialVideo() {
    $('.va-testimonial-video-slider .va-video-play-btn.no-modal').on('click', function () {
        var currentSlide = $(this).parents('.va-testimonial-video-slide');
        currentSlide.addClass('video-active');
        $('.va-testimonial-video-slider-pagination').addClass('pull-down');
    });

    $('.va-testimonial-video-slider-pagination li, .va-testimonial-video-slide__video-wrapper .video-close').on('click', function () {
        $('.va-testimonial-video-slide').each(function () {
            $(this).removeClass('video-active');
        });
        $('.va-testimonial-video-slider-pagination').removeClass('pull-down');
    });
}
// end file: va-testimonial-video-slider.js

// file: Blog/blog.js
$(document).ready(function () {

    $('.post-item .thumb .topic-list .topic-link').each(function () {

        if ($(this).text() === "Alle" || $(this).text() === "All") {
            $(this).css('display', 'none');
        }
    });

    $('.post-body .conntent-thumb .topic-list .topic-link').each(function () {

        if ($(this).text() === "Alle" || $(this).text() === "All") {
            $(this).css('display', 'none');
        }
    });
});
// end file: Blog/blog.js

// file: Modules/lang-detect.js
$(document).ready(function () {

    if ($('body').hasClass('de')) {
        $('.region-language-switch .links .de a').addClass('is-active');
        $('.region-language-switch .links .en a').removeClass('is-active');
    }
    if ($('body').hasClass('en')) {
        $('.region-language-switch .links .en a').addClass('is-active');
        $('.region-language-switch .links .de a').removeClass('is-active');
    }
});
// end file: Modules/lang-detect.js

// file: Modules/lp-hero-ebook_pidas.js
// pidas modification -- all CTA in hero Banner scroll to form

$(document).ready(function () {

    $(".va-hero-lp-ebook .text .scroll-to-form").click(function (e) {

        $('html, body').animate({
            scrollTop: $(".va-quick-subscribe").offset().top
        }, 2000);
    });
});

// end file: Modules/lp-hero-ebook_pidas.js

// file: Modules/menu-pidas.js
/*
 * Stolen from here @link: https://github.com/ykob/shape-overlays
 * transpilled from es6 to plain js
 */
$(document).ready(function () {

    var menuOpened = 'menu--is-opened';
    var subMenuOpened = 'sub-menu--opened';
    var subMenuClosed = 'sub-menu--closed';

    var ease = {
        elasticInOut: function elasticInOut(t) {
            var HALF_PI = 1.5707963267948966;
            return t < 0.5 ? 0.5 * Math.sin(+13.0 * HALF_PI * 2.0 * t) * Math.pow(2.0, 10.0 * (2.0 * t - 1.0)) : 0.5 * Math.sin(-13.0 * HALF_PI * (2.0 * t - 1.0 + 1.0)) * Math.pow(2.0, -10.0 * (2.0 * t - 1.0)) + 1.0;
        },
        exponentialIn: function exponentialIn(t) {
            return t == 0.0 ? t : Math.pow(2.0, 10.0 * (t - 1.0));
        },
        exponentialOut: function exponentialOut(t) {
            return t == 1.0 ? t : 1.0 - Math.pow(2.0, -10.0 * t);
        },
        exponentialInOut: function exponentialInOut(t) {
            return t == 0.0 || t == 1.0 ? t : t < 0.5 ? +0.5 * Math.pow(2.0, 20.0 * t - 10.0) : -0.5 * Math.pow(2.0, 10.0 - t * 20.0) + 1.0;
        },
        sineOut: function sineOut(t) {
            var HALF_PI = 1.5707963267948966;
            return Math.sin(t * HALF_PI);
        },
        circularInOut: function circularInOut(t) {
            return t < 0.5 ? 0.5 * (1.0 - Math.sqrt(1.0 - 4.0 * t * t)) : 0.5 * (Math.sqrt((3.0 - 2.0 * t) * (2.0 * t - 1.0)) + 1.0);
        },
        cubicIn: function cubicIn(t) {
            return t * t * t;
        },
        cubicOut: function cubicOut(t) {
            var f = t - 1.0;
            return f * f * f + 1.0;
        },
        cubicInOut: function cubicInOut(t) {
            return t < 0.5 ? 4.0 * t * t * t : 0.5 * Math.pow(2.0 * t - 2.0, 3.0) + 1.0;
        },
        quadraticOut: function quadraticOut(t) {
            return -t * (t - 2.0);
        },
        quarticOut: function quarticOut(t) {
            return Math.pow(t - 1.0, 3.0) * (1.0 - t) + 1.0;
        }
    };

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) {
                    descriptor.writable = true;
                }
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) {
                defineProperties(Constructor.prototype, protoProps);
            }
            if (staticProps) {
                defineProperties(Constructor, staticProps);
            }
            return Constructor;
        };
    }();

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var ShapeOverlays = function () {
        function ShapeOverlays(elm, clip) {
            _classCallCheck(this, ShapeOverlays);

            this.elm = elm;
            this.path = elm.querySelectorAll('path');
            this.clipRect = clip;
            this.numPoints = 2;
            this.duration = 600;
            this.delayPointsArray = [];
            this.delayPointsMax = 0;
            this.delayPerPath = 100;
            this.timeStart = Date.now();
            this.isOpened = false;
            this.isAnimating = false;
        }

        _createClass(ShapeOverlays, [{
            key: 'toggle',
            value: function toggle() {
                this.isAnimating = true;
                for (var i = 0; i < this.numPoints; i++) {
                    this.delayPointsArray[i] = 0;
                }
                if (this.isOpened === false) {
                    this.open();
                } else {
                    this.close();
                }
            }
        }, {
            key: 'open',
            value: function open() {
                this.isOpened = true;
                this.elm.classList.add('is-opened');
                this.timeStart = Date.now();
                this.renderLoop();
            }
        }, {
            key: 'close',
            value: function close() {
                this.isOpened = false;
                this.elm.classList.remove('is-opened');
                this.timeStart = Date.now();
                this.renderLoop();
            }
        }, {
            key: 'updatePath',
            value: function updatePath(time) {
                var points = [];
                for (var i = 0; i < this.numPoints; i++) {
                    var thisEase = i % 2 === 1 ? ease.cubicIn : ease.cubicIn;
                    points[i] = (1 - thisEase(Math.min(Math.max(time - this.delayPointsArray[i], 0) / this.duration, 1))) * 100;
                }

                var str = '';
                str += this.isOpened ? 'M 0 0 H ' + points[0] : 'M ' + points[0] + ' 0';
                for (var i = 0; i < this.numPoints - 1; i++) {
                    var p = (i + 1) / (this.numPoints - 1) * 100;
                    var cp = (i + 1) / (this.numPoints - 1) * 100;
                    str += 'C ' + points[i] + ' ' + cp + ' ' + points[i + 1] + ' ' + cp + ' ' + points[i + 1] + ' ' + p + ' ';
                }
                // adjusted close position
                str += 'H 100 V 0';
                return str;
            }
        }, {
            key: 'render',
            value: function render() {
                if (this.isOpened) {
                    for (var i = 0; i < this.path.length; i++) {
                        var time = Date.now() - (this.timeStart + this.delayPerPath * i);
                        this.path[i].setAttribute('d', this.updatePath(time));
                        // Follow the last area unfolding.
                        if (i === this.path.length - 1) {
                            var x = 1 - 1 / (this.duration / time);
                            this.clipRect.setAttribute('x', x);
                        }
                    }
                } else {
                    for (var i = 0; i < this.path.length; i++) {
                        // custom timing, to revert direction.
                        var time = this.duration + (this.timeStart - Date.now()) - this.delayPerPath * (this.path.length - i - 1);
                        this.path[i].setAttribute('d', this.updatePath(time));
                        // Follow the last area unfolding.
                        if (i === 1) {
                            var x = 1 - 1 / (this.duration * 1.125 / time);
                            this.clipRect.setAttribute('x', x);
                        }
                    }
                }
            }
        }, {
            key: 'renderLoop',
            value: function renderLoop() {
                var _this = this;

                this.render();
                if (Date.now() - this.timeStart < this.duration + this.delayPerPath * (this.path.length - 1) + this.delayPointsMax) {
                    requestAnimationFrame(function () {
                        _this.renderLoop();
                    });
                } else {
                    this.isAnimating = false;
                }
            }
        }]);
        return ShapeOverlays;
    }();

    function init(context) {
        var elmMenuHandle = context.querySelector('.menu-handle');
        var body = context.body;
        var submenuIndicator = context.querySelectorAll('.sub-menu--indicator');
        var elmOverlay = context.querySelector('.shape-overlays');
        var shapeBackground = context.querySelector('.shape-overlays-bg');
        var navClip = context.querySelector('#nav-clip-rect');
        // console.log(elmMenuHandle);
        if (!elmOverlay) {
            return;
        }

        window.onscroll = function () {
            var scrollPosition = document.documentElement.scrollTop || body.scrollTop;

            if (scrollPosition > 50) {
                elmMenuHandle.classList.add('hide-hint');
                elmMenuHandle.classList.remove('show-hint');
            } else {
                elmMenuHandle.classList.add('show-hint');
                elmMenuHandle.classList.remove('hide-hint');
            }
        };

        var overlay = new ShapeOverlays(elmOverlay, navClip);

        function keyboardClose(event) {
            var keyCode = event.keyCode || event.which;

            if (keyCode === 27) {
                overlay.toggle();
                closeOverlay();

                context.removeEventListener(event.type, keyboardClose);
            }
        }

        function closeOverlay() {
            var opensubmenues = context.querySelectorAll(subMenuOpened);

            // Clean up potential sub menus.
            for (var i = 0; i < opensubmenues.length; i++) {
                opensubmenues[i].classList.remove(subMenuOpened);
            }

            body.classList.remove(menuOpened);
        }

        function openOverlay() {
            body.classList.add(menuOpened);
        }

        for (var i = 0; i < submenuIndicator.length; i++) {
            submenuIndicator[i].addEventListener('click', function (event) {
                event.preventDefault();
                event.stopPropagation();

                if (this.parentNode.parentNode.classList.contains(subMenuOpened)) {
                    this.parentNode.parentNode.classList.remove(subMenuOpened);
                    this.parentNode.parentNode.classList.add(subMenuClosed);
                } else {
                    this.parentNode.parentNode.classList.remove(subMenuClosed);
                    this.parentNode.parentNode.classList.add(subMenuOpened);
                }
            });
        }

        // Background click
        shapeBackground.addEventListener('click', function (event) {
            event.preventDefault();

            overlay.toggle();
            closeOverlay();
        });

        // Burger click
        elmMenuHandle.addEventListener('click', function () {
            if (overlay.isAnimating) {
                return false;
            }

            overlay.toggle();

            if (!overlay.isOpened) {
                closeOverlay();

                return;
            }

            openOverlay();
            // Keyboard Esc
            context.addEventListener('keyup', keyboardClose);
        });
    }
    //Drupal.behaviors.overlayMenu = {
    //  attach: init
    //};
    init(document);
});
// end file: Modules/menu-pidas.js

// file: Modules/smooth-scroll.js
// $(function(){	

//         var $window = $(window);
// 	var scrollTime = 1.2;
// 	var scrollDistance = 170;

// 	$window.on("mousewheel DOMMouseScroll", function(event){

// 		event.preventDefault();	

// 		var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
// 		var scrollTop = $window.scrollTop();
// 		var finalScroll = scrollTop - parseInt(delta*scrollDistance);

// 		TweenMax.to($window, scrollTime, {
// 			scrollTo : { y: finalScroll, autoKill:true },
// 				ease: Power1.easeOut,
// 				overwrite: 5							
// 			});

// 	});
// });
// end file: Modules/smooth-scroll.js

// file: Modules/va-3-posts-section.js
$(document).ready(function () {
    $('.va-3-blog-posts-section__slider').not('.slick-initialized').slick({
        mobileFirst: true,
        infinite: true,
        autoplay: true,
        arrows: false,
        dots: true,
        dotsClass: 'va-slick-dots',
        slidesToShow: 1,
        responsive: [{
            breakpoint: 991,
            settings: "unslick"
        }]
    });

    /* Unslick fix */
    $(window).resize(function () {
        $('.va-3-blog-posts-section__slider').slick('resize');
    });

    $(window).on('orientationchange', function () {
        $('.va-3-blog-posts-section__slider').slick('resize');
    });
});

// end file: Modules/va-3-posts-section.js

// file: Modules/va-action-slider.js
$(document).ready(function () {
    $('.va-action-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
        dots: true,
        infinite: false,
        dotsClass: 'va-slick-dots',
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '33%',
        variableWidth: true,
        variableHeight: true,
        initialSlide: findMiddleSlide('.action-slide'),
        responsive: [{
            breakpoint: 768,
            settings: {
                centerPadding: '0px'
            }
        }, {
            breakpoint: 431,
            settings: {
                variableWidth: false,
                centerPadding: '0px'
            }
        }]
    });
});
// end file: Modules/va-action-slider.js

// file: Modules/va-blog-subscription.js
$(document).ready(function () {
    waitForLoad(".va-blog-subscription", ".cta_button ", function () {
        $('.va-blog-subscription form button').removeClass('cta-tertiary');
    });
});

// end file: Modules/va-blog-subscription.js

// file: Modules/va-counter.js
$(document).ready(function () {
    if ($('.va-counter-wrapper').length > 0) {
        initCounterPlaceholder();
        var countersCount = $('.va-counter').length;
        var count = 0;

        $(window).scroll(function () {
            var scrollTop = $(window).first().scrollTop();
            $('.va-counter').each(function (i) {
                if (i == count) {
                    var offsetTop = $(this).offset().top + $(this).outerHeight() / 2 - window.innerHeight;
                    if (scrollTop > offsetTop) {
                        startCount($(this));
                        count++;
                    }
                }
            });
        });
        $(window).scroll();
    }
});

function initCounterPlaceholder() {
    $('.va-counter__number').each(function () {
        var numberPlaceholder = $(this).find('.va-counter__number-placeholder');
        if (typeof $(this).data('num') == 'string') {
            var dataNum = $(this).data('num');
            var dataParsed = parseInt(dataNum);
            var dataAppendix = dataNum.replace(dataParsed, '');
            if ($(this).find('.va-counter__number-appendix').length < 1) {
                numberPlaceholder.after('<span class="va-counter__number-appendix">' + dataAppendix + '</span>');
            }
        }
    });
}

function startCount(counterClass) {
    var numberPlaceholder = $(counterClass).find('.va-counter__number-placeholder');
    $(numberPlaceholder).prop('Counter', 0).animate({
        Counter: $(counterClass).find('.va-counter__number').data('num')
    }, {
        duration: 4000,
        easing: 'swing',
        step: function step(now) {
            $(numberPlaceholder).text(Math.ceil(now));
        }
    });
}
// end file: Modules/va-counter.js

// file: Modules/va-hero-lp-form.js
$(document).ready(function () {
    $(".va-hero-lp-form .form h3").after($(".va-hero-lp-form  .form p"));

    waitForLoad('.va-hero-lp-form .form-wrapper', 'form', function (wrapper, form) {
        var inputSubmit = form.find('input[type=submit]');
        var date = new Date();
        $(this).attr('id', date.getTime());
        inputSubmit.after('<button class="cta_button cta-tertiary" ' + ' id="id_' + date.getTime() + '">' + inputSubmit.val() + '</button>');
        var react = inputSubmit.attr("data-reactid");
        inputSubmit.next().attr("data-reactid", react);
        inputSubmit.remove();
    });
});

// end file: Modules/va-hero-lp-form.js

// file: Modules/va-hero-slider.js
$(document).ready(function () {
    $('.va-hero-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
        autoplay: true,
        dots: true,
        dotsClass: 'va-hero-slider__dots',
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        vertical: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                dots: false

            }
        }]
    });
});
// end file: Modules/va-hero-slider.js

// file: Modules/va-image-slider.js
$(document).ready(function () {
    $('.va-image-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        dotsClass: 'va-slick-dots',
        arrows: false,
        infinite: true,
        slidesToShow: 1
    });
});
// end file: Modules/va-image-slider.js

// file: Modules/va-maplocation.js
var geocoder;
var map;

// functions init for custom module va-maplocation
function initialize() {
    geocoder = new google.maps.Geocoder();
    var customMapType = new google.maps.StyledMapType([{ "featureType": "all", "stylers": [{ "saturation": 0 }, { "hue": "#e7ecf0" }] }, { "featureType": "road", "stylers": [{ "saturation": -70 }] }, { "featureType": "transit", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }] }]);
    var customMapTypeId = 'custom_style';
    var mapOptions = {
        zoom: 14,
        scrollwheel: false,
        disableDefaultUI: true,
        zoomControl: true,
        fullscreenControl: true,
        streetViewControl: true,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
        }
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    map.mapTypes.set(customMapTypeId, customMapType);
    map.setMapTypeId(customMapTypeId);
    markPlacesFromLocalStorage();
    // goToGeocodeAddress($('.va-ourlocations span > div:first-child .va-ourlocation-single h6').data('location-id'), $('.va-ourlocations span > div:first-child .va-ourlocation-single p').text());
}

function markPlacesFromLocalStorage() {
    localStorage.clear();
    for (var a in localStorage) {
        var temp = localStorage[a].slice(1, -1).split(', ');
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(temp[0], temp[1]),
            icon: 'https://cdn2.hubspot.net/hubfs/685080/HubSpot_Template_Marketplace/Turbo_Vanadyl/icons/marker.png'
        });
    }
}

function goToGeocodeAddress(address_title, address, ifClick) {
    geocoder.geocode({
        'address': address
    }, function (results, status) {

        if (status == 'OK') {
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon: 'https://cdn2.hubspot.net/hubfs/685080/HubSpot_Template_Marketplace/Turbo_Vanadyl/icons/marker.png'
            });
            if (ifClick === true) {
                map.setCenter(results[0].geometry.location);
            }
            localStorage.setItem('place-' + address_title, results[0].geometry.location);
        }
    });
}

var i = 0;
function goToLocalStorageAddress(lat, lng) {
    map.setCenter(new google.maps.LatLng(lat, lng));
}

$(document).ready(function () {
    $('.va-ourlocation-single h6').each(function (index, elem) {
        var location_title = $(this).data('location-id');
        var location = $(this).parent().find('p').text();
        if (localStorage.getItem("place-" + location_title) !== null) {
            var temp = localStorage.getItem('place-' + location_title).slice(1, -1).split(', ');
            goToLocalStorageAddress(temp[0], temp[1]);
        } else {
            if (index === 0) {
                goToGeocodeAddress(location_title, location, true);
            } else {
                goToGeocodeAddress(location_title, location, false);
            }
        }
    });
    $('.va-ourlocation-single h6').click(function (index, elem) {
        var location_title = $(this).data('location-id');
        var location = $(this).parent().find('p').text();
        if (localStorage.getItem("place-" + location_title) !== null) {
            var temp = localStorage.getItem('place-' + location_title).slice(1, -1).split(', ');
            goToLocalStorageAddress(temp[0], temp[1]);
        } else {
            goToGeocodeAddress(location_title, location, true);
        }
    });
    $('.va-ourlocations span > div .va-ourlocation-single').each(function (index, el) {
        $(this).attr('data-list-position', index);
    });
});

// end file: Modules/va-maplocation.js

// file: Modules/va-mocup-slider.js
$(document).ready(function () {
    var screensSlider = $('.va-mocup-slider__screens');
    var phoneSlider = $('.va-mocup-slider__phone-slider');
    var contentSlider = $('.va-mocup-slider__content');
    var slideCount = $('.va-mocup-slide').length;

    $('.va-mocup-slide').each(function () {
        $(this).find('.va-mocup-slide__image').appendTo(screensSlider).clone().appendTo(phoneSlider);
        $(this).find('.va-mocup-slide__content').appendTo(contentSlider);
    });

    if ($('.va-mocup-slide').length > 2) {
        initMocupSlider();
    } else {
        $('.va-mocup-slider').addClass('va-mocup-slider--normal');
        initNormalSlider();
        $('.va-mocup-slider__content .slick-current').addClass('active');
    }

    function initMocupSlider() {

        $(screensSlider).not('.slick-initialized').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            draggable: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.sync-slide',
            dotsClass: 'va-slick-dots',
            dots: true,
            arrows: false,
            centerMode: true,
            variableWidth: true,
            focusOnSelect: true
        });

        $(phoneSlider).not('.slick-initialized').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            draggable: false,
            accessibility: false,
            swipe: false,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            asNavFor: '.sync-slide',
            variableWidth: true
        });

        $(contentSlider).not('.slick-initialized').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            draggable: false,
            accessibility: false,
            swipe: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            dotsClass: 'va-slick-dots',
            asNavFor: '.sync-slide',
            vertical: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    draggable: true,
                    accessibility: true,
                    swipe: true,
                    infinite: true,
                    vertical: false,
                    dots: true
                }
            }]
        });

        $(window).resize(function () {
            setTimeout(function () {
                $('.va-mocup-slider__content .slick-slide[data-slick-index="-1"]').appendTo('.va-mocup-slider__content .slick-track');
                $('.va-mocup-slider__content .slick-slide').removeClass('active');
                $('.va-mocup-slider__content .slick-current').next().addClass('active');
            }, 100);
        }).resize();

        $('.va-mocup-slider__content').on('afterChange', function (event, slick, currentSlide) {
            $('.va-mocup-slider__content .slick-slide').next().removeClass('active');
            $('.va-mocup-slider__content .slick-current').next().addClass('active');
        });
    }

    function initNormalSlider() {

        $(phoneSlider).not('.slick-initialized').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            draggable: false,
            accessibility: false,
            swipe: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            asNavFor: contentSlider,
            variableWidth: true
        });

        $(contentSlider).not('.slick-initialized').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            dotsClass: 'va-slick-dots',
            asNavFor: phoneSlider,
            vertical: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    infinite: true,
                    vertical: false,
                    dots: true
                }
            }]
        });

        $('.va-mocup-slider__content').on('afterChange', function (event, slick, currentSlide) {
            $('.va-mocup-slider__content .slick-slide').removeClass('active');
            $('.va-mocup-slider__content .slick-current').addClass('active');
        });
    }
});
// end file: Modules/va-mocup-slider.js

// file: Modules/va-partner-slider.js
$(document).ready(function () {
    $('.va-partner-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
        autoplay: true,
        dots: false,
        dotsClass: 'va-slick-dots',
        arrows: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 768,
            settings: {
                dots: true,
                slidesToShow: 2
            }
        }, {
            breakpoint: 481,
            settings: {
                dots: true,
                slidesToShow: 1
            }
        }]
    });
});
// end file: Modules/va-partner-slider.js

// file: Modules/va-quick-subscribe.js
$(document).ready(function () {
    $(".va-quick-subscribe .form h3").after($(".va-quick-subscribe .form p"));

    waitForLoad('.va-quick-subscribe', 'form', function (wrapper, form) {
        var inputSubmit = form.find('input[type=submit]');
        var date = new Date();
        inputSubmit.after('<button class="cta_button cta-tertiary" id="_' + date.getTime() + '">' + inputSubmit.val() + '</button>');
        inputSubmit.remove();
    });
});
// end file: Modules/va-quick-subscribe.js

// file: Modules/va-testimonial-slider.js
$(document).ready(function () {
    $('.va-testimonial-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        dotsClass: 'va-slick-dots',
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        responsive: [{
            breakpoint: 768,
            settings: {
                dots: false

            }
        }]
    });
});
// end file: Modules/va-testimonial-slider.js

// file: Website/buttonIEHack.js
function buttonIEHack() {
    var wrapper = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var form = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";

    var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
    if (isIE) {

        if (wrapper == "") {
            $(".cta-primary, .cta-secondary, .cta-tertiary").each(function () {
                var id = $(this).attr('id');
                if (!id) {
                    var time = new Date();
                    $(this).attr('id', 'id_' + time.getTime());
                }
                $('<style type="text/css">#' + id + ':hover:before, #' + $(this).attr('id') + ':hover:after{width: ' + parseInt($(this).outerWidth() + 20) + 'px!important; height: ' + parseInt($(this).outerHeight() + 20) + 'px!important;}</style>').appendTo('head');
            });
        } else {
            var element = wrapper.find(".cta-primary, .cta-secondary, .cta-tertiary");
            var id = element.attr("id");
            $('<style type="text/css">#' + id + ':hover:before, #' + element.attr('id') + ':hover:after{width: ' + parseInt(element.outerWidth() + 20) + 'px!important; height: ' + parseInt(element.outerHeight() + 20) + 'px!important;}</style>').appendTo('head');
        }
    }
}
waitForLoad(".hs-cta-wrapper", ".cta_button ", function (wrapper, form) {
    buttonIEHack("", "");
});
waitForLoad(".widget-type-form,.widget-type-blog_content", "form", function (wrapper, form) {
    buttonIEHack("", "");
});
waitForLoad(".hs_cos_wrapper_type_form, .hs_cos_wrapper_type_blog_comments, .hs_cos_wrapper_type_blog_subscribe", "form", function (wrapper, form) {
    var inputSubmit = form.find('input[type=submit]');
    var date = new Date();
    $(this).attr('id', date.getTime());
    inputSubmit.after('<button class="cta_button cta-tertiary" ' + ' id="id_' + date.getTime() + '">' + inputSubmit.val() + '</button>');
    var react = inputSubmit.attr("data-reactid");
    inputSubmit.next().attr("data-reactid", react);
    inputSubmit.remove();

    buttonIEHack(wrapper, form);
});

// end file: Website/buttonIEHack.js

// file: Website/globalFormIChuj.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
waitForLoad(".widget-type-form,.widget-type-blog_content,.widget-type-blog_subscribe", "form", formError);

function formError() {
    $("form input, form select, form textarea").change(function () {
        var $this = $(this);
        setTimeout(function () {
            var error = $this.parent().next();
            if (error.hasClass("hs-error-msgs") && error != undefined) {
                $this.parent().addClass("error-border");
                $this.parent().removeClass("verify");
            } else {
                $this.parent().removeClass("error-border");
                $this.parent().addClass("verify");
            }
        }, 300);
    });

    $("form input, form select, form textarea").focusout(function (e) {
        var $this = $(this);
        setTimeout(function () {
            var error = $this.parent().next();
            if (error.hasClass("hs-error-msgs") && error != undefined) {
                $this.parent().addClass("error-border");
                $this.parent().removeClass("verify");
            } else {
                $this.parent().addClass("verify");
                $this.parent().removeClass("error-border");
            }
        }, 300);
    });
}

// Functions for dropdown
function select() {
    $("select").each(function () {
        var parent = $(this).parent();
        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        $(this).find('option').each(function () {
            if ($(this).val() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>');
            }
        });
    });

    $('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

//file upload
function fileUpload() {
    $('input[type=file]').each(function (i, e) {
        $(this).parent().addClass('file-upload');
        $(this).parent().append('<div class="overlay"><span>Drop file here</span></div>');
        $(this).parent().find('.overlay').append('<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-929 271 60 60" style="enable-background:new -929 271 60 60;" xml:space="preserve"><g><path d="M-884,296h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,296-884,296z"/><path d="M-909,290h10c0.6,0,1-0.4,1-1s-0.4-1-1-1h-10c-0.6,0-1,0.4-1,1S-909.6,290-909,290z"/><path d="M-884,304h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,304-884,304z"/><path d="M-884,312h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,312-884,312z"/><path d="M-884,320h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,320-884,320z"/><path d="M-880,285.6V271h-43v55h5v5h43v-40.4L-880,285.6z M-889,279.4l9,9l1.6,1.6H-889V279.4z M-921,324v-51h39v10.6l-7.6-7.6H-918v48H-921z M-916,329v-3v-48h25v14h14v37H-916z"/></g></svg>');
        $(this).change(function () {
            $(e).siblings(".overlay").find("span").text($(e).val().split("\\")[2]);
        });
    });
}

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).addClass('hidden-label');
        }
    });
}

var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
if (iOS) {
    $('body').addClass('ios');
}

waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .hs_cos_wrapper_type_form, .hs_cos_wrapper_type_blog_subscribe", "form", hideEmptyLabel);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", fileUpload);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", formError);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", function () {
    $('form select').on('change', function () {
        $('form .dropdown-header').addClass('dropdown-selected');
    });
});

// end file: Website/globalFormIChuj.js

// file: Website/pidas-resources-page.js
// scroll top
function scrollTop() {
    $("html, body").animate({
        scrollTop: $(".resources-item-wrapper ").position().top - 0
    }, 1000);
}

// pagination 
function pagination() {

    // all content number 
    var totalContent = $(".resources-item-wrapper > span > div.after-search").length;

    var getNumberOfPage = '';

    if ($('.item-per-page-settings').length != 0) {

        getNumberOfPage = $('.item-per-page-settings span').text(); //4;
    } else {
        getNumberOfPage = 6;
    }

    var posPerPage = parseInt(getNumberOfPage);

    var totalPage = Math.round(totalContent / posPerPage);

    // generte page and pagination k
    if (posPerPage < totalContent) {

        $(".resources-item-wrapper > span > div.after-search:gt(" + (posPerPage - 1) + ")").hide();

        for (i = 0; i <= totalPage - 1; i++) {
            console.log(totalPage);
            $(".pagination ul").append("<li><a href='javascript:void(0)'>" + (i + 1) + "</a></li>");
        }
    }

    // first page added active class
    $(".pagination li:first").addClass("active");

    // click function
    $(".pagination li").on("click", function () {

        $(".resources-item-wrapper").css('opacity', '0');

        var index = $(this).index() + 1;
        var gt = posPerPage * index;

        $(".pagination li").removeClass("active");
        $(this).addClass("active");

        setTimeout(function () {

            $(".resources-item-wrapper > span > div.after-search").hide();

            for (i = gt - posPerPage; i < gt; i++) {

                $(".resources-item-wrapper > span > div.after-search:eq(" + i + ")").show();
            }
        }, 300);

        setTimeout(function () {
            $(".resources-item-wrapper").css('opacity', '1');
            scrollTop();
        }, 300);
    });
}
// custom search
function customSearch() {

    var searchContent = '';

    $('#search-input').keyup(function (e) {

        var searchedword = e.target.value.toLowerCase();
        var books = $('.resources-single-item');

        // CLEAR PAGINATION 
        $('.pagination ul > li').remove();
        $(".no-results").remove();

        books.each(function () {

            searchContent = $(this).find('h3 a').text().toLowerCase() + $(this).find('p span').text().toLowerCase();

            if (searchContent.indexOf(searchedword) != -1) {
                $(this).parent().css('display', 'block');
                $(this).parent().addClass('after-search');
            } else {
                $(this).parent().css('display', 'none');
                $(this).parent().removeClass('after-search');
            }
            searchContent = '';
        });

        if ($(".resources-item-wrapper > span > div.after-search").length == 0) {
            $(".resources-item-wrapper  ").parent().append('<h2 class="no-results" >No Results...</h2>');
        }

        pagination();
    });
}
// sort by Category
function sortbyCategory() {

    $(".cat-menu li a").on("click", function (e) {
        e.preventDefault();

        $(".resources-item-wrapper").css('opacity', '0');

        var catName = $(this).text().toLowerCase();
        var item = '';
        // console.log(catName);

        $('.pagination ul > li').remove();
        $(".no-results").remove();

        setTimeout(function () {

            $('.resources-item-wrapper > span > div').each(function () {

                item = $(this).find('.resources-single-item').attr('data-category').toLowerCase();

                $(this).removeClass('show-by-cat').removeClass('show-by-cat');
                $(this).css('display', 'none');

                if (catName === item) {
                    // console.log(this);
                    $(this).css('display', 'block');
                    $(this).addClass('after-search');
                } else if (catName === "all") {
                    $(this).css('display', 'block');
                    $(this).addClass('after-search');
                } else {
                    $(this).css('display', 'none');
                    $(this).removeClass('after-search');
                }
            });

            pagination();

            $(".resources-item-wrapper").css('opacity', '1');
            scrollTop();
        }, 300);
    });
}

$(document).ready(function () {
    customSearch();
    // first load all elments is after search 
    $(".resources-item-wrapper > span > div").addClass('after-search');
    pagination();
    sortbyCategory();
});

// end file: Website/pidas-resources-page.js

// file: Website/va-108-solutions.js
$(document).ready(function () {
    $('.va-company .slider-content > span').slick({
        autoplay: true,
        dots: true,
        dotsClass: 'va-slick-dots',
        arrows: false
    });
});
// end file: Website/va-108-solutions.js

// file: Website/va-503+505-subscription-settings.js
$(window).load(function () {
    $('.va-subscription-settings-update .forms-section #submitbutton').wrap('<span class="cta_button cta-secondary"></span>');
    $('.va-subscription-settings-backup .backup-section #submitbutton').wrap('<span class="cta_button cta-tertiary"></span>');
});
// end file: Website/va-503+505-subscription-settings.js

// file: Website/va-contact.js
$(document).ready(function () {
    waitForLoad('.widget-type-form', 'form', function (wrapper, form) {
        var inputSubmit = form.find('input[type=submit]');
        var date = new Date();
        inputSubmit.after('<button class="cta_button cta-tertiary" + id="_' + date + '">' + inputSubmit.val() + '</button>');
        inputSubmit.remove();
    });
});

// end file: Website/va-contact.js

// file: Website/va-hero-lp-ebook.js
$(document).ready(function () {
    heroLPHeight();
});

$(window).resize(function () {
    heroLPHeight();
});
$(window).load(function () {
    heroLPHeight();
});

var heroLPHeight = function heroLPHeight() {
    $("body:not(.shortened) .va-hero-lp-ebook").height($(".va-hero-lp-ebook .content").height());
};

// end file: Website/va-hero-lp-ebook.js
//# sourceMappingURL=template.js.map
