var geocoder;
var map;

// functions init for custom module va-maplocation
function initialize() {
    geocoder = new google.maps.Geocoder();
    var customMapType = new google.maps.StyledMapType([{ "featureType": "all", "stylers": [{ "saturation": 0 }, { "hue": "#e7ecf0" }] }, { "featureType": "road", "stylers": [{ "saturation": -70 }] }, { "featureType": "transit", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }] }]);
    var customMapTypeId = 'custom_style';
    var mapOptions = {
        zoom: 14,
        scrollwheel: false,
        disableDefaultUI: true,
        zoomControl: true,
        fullscreenControl: true,
        streetViewControl: true,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
        }
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    map.mapTypes.set(customMapTypeId, customMapType);
    map.setMapTypeId(customMapTypeId);
    markPlacesFromLocalStorage();
    // goToGeocodeAddress($('.va-ourlocations span > div:first-child .va-ourlocation-single h6').data('location-id'), $('.va-ourlocations span > div:first-child .va-ourlocation-single p').text());
}

function markPlacesFromLocalStorage() {
    localStorage.clear();
    for (var a in localStorage) {
        var temp = localStorage[a].slice(1, -1).split(', ');
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(temp[0], temp[1]),
            icon: 'https://cdn2.hubspot.net/hubfs/685080/HubSpot_Template_Marketplace/Turbo_Vanadyl/icons/marker.png'
        });
    }
}

function goToGeocodeAddress(address_title, address, ifClick) {
    geocoder.geocode({
        'address': address
    }, function (results, status) {

        if (status == 'OK') {
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon: 'https://cdn2.hubspot.net/hubfs/685080/HubSpot_Template_Marketplace/Turbo_Vanadyl/icons/marker.png'
            });
            if(ifClick === true) {
                map.setCenter(results[0].geometry.location);
            }
            localStorage.setItem('place-' + address_title, results[0].geometry.location);
        }
    });
}

var i = 0;
function goToLocalStorageAddress(lat, lng) {
  map.setCenter(new google.maps.LatLng(lat, lng));
}

$(document).ready(function() {
    $('.va-ourlocation-single h6').each(function(index, elem) {
        var location_title = $(this).data('location-id');
        var location = $(this).parent().find('p').text();
        if (localStorage.getItem("place-" + location_title) !== null) {
            var temp = localStorage.getItem('place-' + location_title).slice(1, -1).split(', ');
            goToLocalStorageAddress(temp[0], temp[1]);
        } else {
            if(index === 0) {
                goToGeocodeAddress(location_title, location, true);
            }
            else {
                goToGeocodeAddress(location_title, location, false);
            }
        }
    });
    $('.va-ourlocation-single h6').click(function(index, elem) {
        var location_title = $(this).data('location-id');
        var location = $(this).parent().find('p').text();
        if (localStorage.getItem("place-" + location_title) !== null) {
            var temp = localStorage.getItem('place-' + location_title).slice(1, -1).split(', ');
            goToLocalStorageAddress(temp[0], temp[1]);
        } else {
            goToGeocodeAddress(location_title, location, true);
        }
    });
    $('.va-ourlocations span > div .va-ourlocation-single').each(function (index, el) {
        $(this).attr('data-list-position', index);
    });
});
