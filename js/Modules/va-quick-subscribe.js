$(document).ready(function(){
	$(".va-quick-subscribe .form h3").after($(".va-quick-subscribe .form p"));

	    waitForLoad('.va-quick-subscribe', 'form', function(wrapper, form) {
        var inputSubmit = form.find('input[type=submit]');
        var date = new Date();
        inputSubmit.after('<button class="cta_button cta-tertiary" id="_'+date.getTime()+'">' + inputSubmit.val() + '</button>');
        inputSubmit.remove();
    });
});