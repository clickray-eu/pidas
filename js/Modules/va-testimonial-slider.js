$(document).ready(function() {
	$('.va-testimonial-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
	  autoplay: true,
	  autoplaySpeed: 5000,
	  dots: false,
	  dotsClass: 'va-slick-dots',
	  arrows: false,
	  infinite: true,
	  slidesToShow: 1,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        dots: false

	      }
	    }
	  ]
	});
});