$(document).ready(function() {
	$('.va-3-blog-posts-section__slider').not('.slick-initialized').slick({
	  mobileFirst: true,
	  infinite: true,
	  autoplay: true,
	  arrows: false,
	  dots: true,
	  dotsClass: 'va-slick-dots',
	  slidesToShow: 1,
	  responsive: [
	    {
	      breakpoint: 991,
	      settings: "unslick"
	    }
	  ]
	});

	/* Unslick fix */
	$(window).resize(function() {
	  $('.va-3-blog-posts-section__slider').slick('resize');
	});

	$(window).on('orientationchange', function() {
	  $('.va-3-blog-posts-section__slider').slick('resize');
	});
});
