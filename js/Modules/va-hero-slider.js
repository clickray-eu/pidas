$(document).ready(function() {
	$('.va-hero-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
	  autoplay: true,
	  dots: true,
	  dotsClass: 'va-hero-slider__dots',
	  arrows: false,
	  infinite: true,
	  slidesToShow: 1,
	  vertical: true,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        dots: false

	      }
	    }
	  ]
	});
});