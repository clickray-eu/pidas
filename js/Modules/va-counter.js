$(document).ready(function() {
    if ($('.va-counter-wrapper').length > 0) {
        initCounterPlaceholder();
        var countersCount = $('.va-counter').length;
        var count = 0;

        $(window).scroll(function() {
            var scrollTop = $(window).first().scrollTop();        
            $('.va-counter').each(function(i) {
                if (i == count) {
                    var offsetTop = $(this).offset().top + $(this).outerHeight()/2 - window.innerHeight;
                    if (scrollTop > offsetTop){
                        startCount($(this));
                        count++;
                    }
                }

            });

        });
        $(window).scroll();
    }
});

function initCounterPlaceholder() {
    $('.va-counter__number').each(function() {
        var numberPlaceholder = $(this).find('.va-counter__number-placeholder');
        if (typeof $(this).data('num') == 'string') {
            var dataNum = $(this).data('num');
            var dataParsed = parseInt(dataNum)
            var dataAppendix = dataNum.replace(dataParsed, '');
            if ($(this).find('.va-counter__number-appendix').length < 1) {
                numberPlaceholder.after('<span class="va-counter__number-appendix">' + dataAppendix + '</span>');
            }
        }
    });
}

function startCount(counterClass) {
    var numberPlaceholder = $(counterClass).find('.va-counter__number-placeholder');
    $(numberPlaceholder).prop('Counter',0).animate({
        Counter: $(counterClass).find('.va-counter__number').data('num')
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(numberPlaceholder).text(Math.ceil(now));
        }
    });
}