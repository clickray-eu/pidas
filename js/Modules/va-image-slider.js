$(document).ready(function() {
	$('.va-image-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
	  autoplay: true,
	  autoplaySpeed: 5000,
	  dots: true,
	  dotsClass: 'va-slick-dots',
	  arrows: false,
	  infinite: true,
	  slidesToShow: 1
	});
});