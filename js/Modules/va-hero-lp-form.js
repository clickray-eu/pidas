$(document).ready(function() {
    $(".va-hero-lp-form .form h3").after($(".va-hero-lp-form  .form p"));

    waitForLoad('.va-hero-lp-form .form-wrapper', 'form', function(wrapper, form) {
        var inputSubmit = form.find('input[type=submit]');
        var date = new Date();
        $(this).attr('id', date.getTime());
        inputSubmit.after('<button class="cta_button cta-tertiary" '+' id="id_' + date.getTime() + '">' + inputSubmit.val() + '</button>');
        var react=inputSubmit.attr("data-reactid");
    	inputSubmit.next().attr("data-reactid",react);
    	inputSubmit.remove();
    });
});




