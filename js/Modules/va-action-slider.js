$(document).ready(function() {
  $('.va-action-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
    dots: true,
    infinite: false,
    dotsClass: 'va-slick-dots',
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '33%',
    variableWidth: true,
    variableHeight: true,
    initialSlide: findMiddleSlide('.action-slide'),
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerPadding: '0px'
        }
      },
      {
        breakpoint: 431,
        settings: {
          variableWidth: false,
          centerPadding: '0px'
        }
      }
    ]
  });
});