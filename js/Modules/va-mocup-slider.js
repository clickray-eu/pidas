$(document).ready(function() {
	var screensSlider = $('.va-mocup-slider__screens');
	var phoneSlider = $('.va-mocup-slider__phone-slider')
	var contentSlider = $('.va-mocup-slider__content');
	var slideCount = $('.va-mocup-slide').length;

	$('.va-mocup-slide').each(function() {
		$(this).find('.va-mocup-slide__image').appendTo(screensSlider).clone().appendTo(phoneSlider);
		$(this).find('.va-mocup-slide__content').appendTo(contentSlider);
	});

	if ($('.va-mocup-slide').length > 2) {
		initMocupSlider();
	} else {
		$('.va-mocup-slider').addClass('va-mocup-slider--normal');
		initNormalSlider()
		$('.va-mocup-slider__content .slick-current').addClass('active');
	}

	function initMocupSlider() {

		$(screensSlider).not('.slick-initialized').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			draggable: false,
			infinite: true,
		    slidesToShow: 1,
		    slidesToScroll: 1,
			asNavFor: '.sync-slide',
			dotsClass: 'va-slick-dots',
			dots: true,
			arrows: false,
			centerMode: true,
		    variableWidth: true,
		    focusOnSelect: true
		});

		$(phoneSlider).not('.slick-initialized').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			draggable: false,
			accessibility: false,
			swipe: false,
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			asNavFor: '.sync-slide',
		    variableWidth: true
		});

		$(contentSlider).not('.slick-initialized').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			draggable: false,
			accessibility: false,
			swipe: false,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			dotsClass: 'va-slick-dots',
			asNavFor: '.sync-slide',
		    vertical: true,
		    responsive: [
				{
			    	breakpoint: 992,
			    	settings: {
			    		draggable: true,
			    		accessibility: true,
						swipe: true,
			    		infinite: true,
			    		vertical: false,
			    		dots: true,
			    	}
			    }
			]
		});

		$(window).resize(function() {
			setTimeout(function() {
				$('.va-mocup-slider__content .slick-slide[data-slick-index="-1"]').appendTo('.va-mocup-slider__content .slick-track');
				$('.va-mocup-slider__content .slick-slide').removeClass('active');
				$('.va-mocup-slider__content .slick-current').next().addClass('active');
			},100)
		}).resize();

		$('.va-mocup-slider__content').on('afterChange', function(event, slick, currentSlide) {
			$('.va-mocup-slider__content .slick-slide').next().removeClass('active');
			$('.va-mocup-slider__content .slick-current').next().addClass('active');
		});
	}

	function initNormalSlider() {

		$(phoneSlider).not('.slick-initialized').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			draggable: false,
			accessibility: false,
			swipe: false,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			asNavFor: contentSlider,
		    variableWidth: true
		});


		$(contentSlider).not('.slick-initialized').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
			dotsClass: 'va-slick-dots',
			asNavFor: phoneSlider,
		    vertical: true,
		    responsive: [
				{
			    	breakpoint: 992,
			    	settings: {
			    		infinite: true,
			    		vertical: false,
			    		dots: true,
			    	}
			    }
			]
		});

		$('.va-mocup-slider__content').on('afterChange', function(event, slick, currentSlide) {
			$('.va-mocup-slider__content .slick-slide').removeClass('active');
			$('.va-mocup-slider__content .slick-current').addClass('active');
		});
	}
});