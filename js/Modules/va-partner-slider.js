$(document).ready(function() {
	$('.va-partner-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
	  autoplay: true,
	  dots: false,
	  dotsClass: 'va-slick-dots',
	  arrows: false,
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        dots: true,
	        slidesToShow: 2
	      }
	    },
	    {
	      breakpoint: 481,
	      settings: {
	        dots: true,
	        slidesToShow: 1
	      }
	    }
	  ]
	});
});