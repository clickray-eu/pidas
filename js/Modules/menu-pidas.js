/*
 * Stolen from here @link: https://github.com/ykob/shape-overlays
 * transpilled from es6 to plain js
 */
$(document).ready(function(){

  var menuOpened = 'menu--is-opened';
  var subMenuOpened = 'sub-menu--opened';
  var subMenuClosed = 'sub-menu--closed';

  var ease = {
    elasticInOut: function elasticInOut(t) {
      var HALF_PI = 1.5707963267948966;
      return t < 0.5
        ? 0.5 * Math.sin(+13.0 * HALF_PI * 2.0 * t) * Math.pow(2.0, 10.0 * (2.0 * t - 1.0))
        : 0.5 * Math.sin(-13.0 * HALF_PI * ((2.0 * t - 1.0) + 1.0)) * Math.pow(2.0, -10.0 * (2.0 * t - 1.0)) + 1.0;
    },
    exponentialIn: function exponentialIn(t) {
      return t == 0.0 ? t : Math.pow(2.0, 10.0 * (t - 1.0));
    },
    exponentialOut: function exponentialOut(t) {
      return t == 1.0 ? t : 1.0 - Math.pow(2.0, -10.0 * t);
    },
    exponentialInOut: function exponentialInOut(t) {
      return t == 0.0 || t == 1.0 ? t : t < 0.5 ? +0.5 * Math.pow(2.0, 20.0 * t - 10.0) : -0.5 * Math.pow(2.0, 10.0 - t * 20.0) + 1.0;
    },
    sineOut: function sineOut(t) {
      var HALF_PI = 1.5707963267948966;
      return Math.sin(t * HALF_PI);
    },
    circularInOut: function circularInOut(t) {
      return t < 0.5 ? 0.5 * (1.0 - Math.sqrt(1.0 - 4.0 * t * t)) : 0.5 * (Math.sqrt((3.0 - 2.0 * t) * (2.0 * t - 1.0)) + 1.0);
    },
    cubicIn: function cubicIn(t) {
      return t * t * t;
    },
    cubicOut: function cubicOut(t) {
      var f = t - 1.0;
      return f * f * f + 1.0;
    },
    cubicInOut: function cubicInOut(t) {
      return t < 0.5 ? 4.0 * t * t * t : 0.5 * Math.pow(2.0 * t - 2.0, 3.0) + 1.0;
    },
    quadraticOut: function quadraticOut(t) {
      return -t * (t - 2.0);
    },
    quarticOut: function quarticOut(t) {
      return Math.pow(t - 1.0, 3.0) * (1.0 - t) + 1.0;
    }
  };

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) {
          descriptor.writable = true;
        }
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) {
        defineProperties(Constructor.prototype, protoProps);
      }
      if (staticProps) {
        defineProperties(Constructor, staticProps);
      }
      return Constructor;
    };
  }();

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var ShapeOverlays = function () {
    function ShapeOverlays(elm, clip) {
      _classCallCheck(this, ShapeOverlays);

      this.elm = elm;
      this.path = elm.querySelectorAll('path');
      this.clipRect = clip;
      this.numPoints = 2;
      this.duration = 600;
      this.delayPointsArray = [];
      this.delayPointsMax = 0;
      this.delayPerPath = 100;
      this.timeStart = Date.now();
      this.isOpened = false;
      this.isAnimating = false;
    }

    _createClass(ShapeOverlays, [{
      key: 'toggle',
      value: function toggle() {
        this.isAnimating = true;
        for (var i = 0; i < this.numPoints; i++) {
          this.delayPointsArray[i] = 0;
        }
        if (this.isOpened === false) {
          this.open();
        }
        else {
          this.close();
        }
      }
    }, {
      key: 'open',
      value: function open() {
        this.isOpened = true;
        this.elm.classList.add('is-opened');
        this.timeStart = Date.now();
        this.renderLoop();
      }
    }, {
      key: 'close',
      value: function close() {
        this.isOpened = false;
        this.elm.classList.remove('is-opened');
        this.timeStart = Date.now();
        this.renderLoop();
      }
    }, {
      key: 'updatePath',
      value: function updatePath(time) {
        var points = [];
        for (var i = 0; i < this.numPoints; i++) {
          var thisEase = i % 2 === 1 ? ease.cubicIn : ease.cubicIn;
          points[i] = (1 - thisEase(Math.min(Math.max(time - this.delayPointsArray[i], 0) / this.duration, 1))) * 100;
        }

        var str = '';
        str += this.isOpened ? 'M 0 0 H ' + points[0] : 'M ' + points[0] + ' 0';
        for (var i = 0; i < this.numPoints - 1; i++) {
          var p = (i + 1) / (this.numPoints - 1) * 100;
          var cp = (i + 1) / (this.numPoints - 1) * 100;
          str += 'C ' + points[i] + ' ' + cp + ' ' + points[i + 1] + ' ' + cp + ' ' + points[i + 1] + ' ' + p + ' ';
        }
        // adjusted close position
        str += 'H 100 V 0';
        return str;
      }
    }, {
      key: 'render',
      value: function render() {
        if (this.isOpened) {
          for (var i = 0; i < this.path.length; i++) {
            var time = Date.now() - (this.timeStart + this.delayPerPath * i);
            this.path[i].setAttribute('d', this.updatePath(time));
            // Follow the last area unfolding.
            if (i === this.path.length - 1) {
              var x = 1 - (1 / (this.duration / time));
              this.clipRect.setAttribute('x', x);
            }
          }
        }
        else {
          for (var i = 0; i < this.path.length; i++) {
            // custom timing, to revert direction.
            var time = this.duration + (this.timeStart - Date.now()) - this.delayPerPath * (this.path.length - i - 1);
            this.path[i].setAttribute('d', this.updatePath(time));
            // Follow the last area unfolding.
            if (i === 1) {
              var x = 1 - (1 / (this.duration * 1.125 / time));
              this.clipRect.setAttribute('x', x);
            }
          }
        }
      }
    }, {
      key: 'renderLoop',
      value: function renderLoop() {
        var _this = this;

        this.render();
        if (Date.now() - this.timeStart < this.duration + this.delayPerPath * (this.path.length - 1) + this.delayPointsMax) {
          requestAnimationFrame(function () {
            _this.renderLoop();
          });
        }
        else {
          this.isAnimating = false;
        }
      }
    }]);
    return ShapeOverlays;
  }();


  function init(context) {
    var elmMenuHandle = context.querySelector('.menu-handle');
    var body = context.body;
    var submenuIndicator = context.querySelectorAll('.sub-menu--indicator');
    var elmOverlay = context.querySelector('.shape-overlays');
    var shapeBackground = context.querySelector('.shape-overlays-bg');
    var navClip = context.querySelector('#nav-clip-rect');
    // console.log(elmMenuHandle);
    if (!elmOverlay) {
      return;
    }

    window.onscroll = function() {
      var scrollPosition = document.documentElement.scrollTop || body.scrollTop;

      if (scrollPosition > 50) {
        elmMenuHandle.classList.add('hide-hint');
        elmMenuHandle.classList.remove('show-hint');
      } else {
        elmMenuHandle.classList.add('show-hint');
        elmMenuHandle.classList.remove('hide-hint');
      }
    };

    var overlay = new ShapeOverlays(elmOverlay, navClip);

    function keyboardClose(event) {
      var keyCode = event.keyCode || event.which;

      if (keyCode === 27) {
        overlay.toggle();
        closeOverlay();

        context.removeEventListener(event.type, keyboardClose);
      }
    }

    function closeOverlay() {
      var opensubmenues = context.querySelectorAll(subMenuOpened);

      // Clean up potential sub menus.
      for (var i = 0; i < opensubmenues.length; i++) {
        opensubmenues[i].classList.remove(subMenuOpened);
      }

      body.classList.remove(menuOpened);
    }

    function openOverlay() {
      body.classList.add(menuOpened);
    }

    for (var i = 0; i < submenuIndicator.length; i++) {
      submenuIndicator[i].addEventListener('click', function (event) {
        event.preventDefault();
        event.stopPropagation();

        if (this.parentNode.parentNode.classList.contains(subMenuOpened)) {
          this.parentNode.parentNode.classList.remove(subMenuOpened);
          this.parentNode.parentNode.classList.add(subMenuClosed);
        } else {
          this.parentNode.parentNode.classList.remove(subMenuClosed);
          this.parentNode.parentNode.classList.add(subMenuOpened);
        }
      });
    }

    // Background click
    shapeBackground.addEventListener('click', function (event) {
      event.preventDefault();

      overlay.toggle();
      closeOverlay();
    });

    // Burger click
    elmMenuHandle.addEventListener('click', function () {
      if (overlay.isAnimating) {
        return false;
      }

      overlay.toggle();

      if (!overlay.isOpened) {
        closeOverlay();

        return;
      }

      openOverlay();
      // Keyboard Esc
      context.addEventListener('keyup', keyboardClose);
    });
  }
  //Drupal.behaviors.overlayMenu = {
  //  attach: init
  //};
  init(document);
});