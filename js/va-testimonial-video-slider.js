$(document).ready(function() {
  $('.va-testimonial-video-slider .hs_cos_wrapper_type_widget_container').not('.slick-initialized').slick({
    accessability: false,
    swipe: false,
    draggeable: false,
    dots: true,
    infinite: false,
    dotsClass: 'va-testimonial-video-slider-pagination',
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: findMiddleSlide('.va-testimonial-video-slide'),
    vertical: true
  });

  facePaginationInit();
  showTestimonialVideo();
});

function facePaginationInit() {
  $('.va-testimonial-video-slider .slick-initialized .slick-slide').each(function() {
    var slideID = $(this).attr('aria-describedby');
    var avatarCompany = $(this).find('.hidden *');
    avatarCompany.appendTo($('.va-testimonial-video-slider-pagination li#' + slideID));
  });
}

function showTestimonialVideo() {
  $('.va-testimonial-video-slider .va-video-play-btn.no-modal').on('click', function() {
    var currentSlide = $(this).parents('.va-testimonial-video-slide');
    currentSlide.addClass('video-active');
    $('.va-testimonial-video-slider-pagination').addClass('pull-down');
  });

  $('.va-testimonial-video-slider-pagination li, .va-testimonial-video-slide__video-wrapper .video-close').on('click', function() {
    $('.va-testimonial-video-slide').each(function() {
      $(this).removeClass('video-active');
    });
    $('.va-testimonial-video-slider-pagination').removeClass('pull-down');
  });
}