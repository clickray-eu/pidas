function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function(i, el) {
            var waitForLoad = setInterval(function() {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
waitForLoad(".widget-type-form,.widget-type-blog_content,.widget-type-blog_subscribe", "form", formError);

function formError() {
    $("form input, form select, form textarea").change(function() {
        var $this = $(this);
        setTimeout(function() {
            var error = $this.parent().next();
            if (error.hasClass("hs-error-msgs") && error != undefined) {
                $this.parent().addClass("error-border");
                $this.parent().removeClass("verify");
            } else {
                $this.parent().removeClass("error-border");
                $this.parent().addClass("verify");
            }
        }, 300);
    });


    $("form input, form select, form textarea").focusout(function(e) {
        var $this = $(this);
        setTimeout(function() {
            var error = $this.parent().next();
            if (error.hasClass("hs-error-msgs") && error != undefined) {
                $this.parent().addClass("error-border");
                $this.parent().removeClass("verify");
            } else {
                $this.parent().addClass("verify");
                $this.parent().removeClass("error-border");
            }
        }, 300);
    });
}

// Functions for dropdown
function select() {
    $("select").each(function() {
        var parent = $(this).parent();
        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        $(this).find('option').each(function() {
            if ($(this).val() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>')
            }
        })
    })

    $('.dropdown_select.input .dropdown-header').click(function(event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $('.dropdown-list li').click(function() {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $('.dropdown_select .input').click(function(event) {
        event.stopPropagation();
    });
}


//file upload
function fileUpload() {
    $('input[type=file]').each(function(i, e) {
        $(this).parent().addClass('file-upload');
        $(this).parent().append('<div class="overlay"><span>Drop file here</span></div>');
        $(this).parent().find('.overlay').append('<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-929 271 60 60" style="enable-background:new -929 271 60 60;" xml:space="preserve"><g><path d="M-884,296h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,296-884,296z"/><path d="M-909,290h10c0.6,0,1-0.4,1-1s-0.4-1-1-1h-10c-0.6,0-1,0.4-1,1S-909.6,290-909,290z"/><path d="M-884,304h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,304-884,304z"/><path d="M-884,312h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,312-884,312z"/><path d="M-884,320h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,320-884,320z"/><path d="M-880,285.6V271h-43v55h5v5h43v-40.4L-880,285.6z M-889,279.4l9,9l1.6,1.6H-889V279.4z M-921,324v-51h39v10.6l-7.6-7.6H-918v48H-921z M-916,329v-3v-48h25v14h14v37H-916z"/></g></svg>');
        $(this).change(function() {
            $(e).siblings(".overlay").find("span").text($(e).val().split("\\")[2]);
        })
    })
}

function hideEmptyLabel() {
    $('form label').each(function(i, e) {
        if ($(this).text() == "*") {
            $(this).addClass('hidden-label');
        }
    })
}

var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
if (iOS) {
    $('body').addClass('ios');
}

waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .hs_cos_wrapper_type_form, .hs_cos_wrapper_type_blog_subscribe", "form", hideEmptyLabel);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", fileUpload);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", formError);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", function() {
    $('form select').on('change', function() {
        $('form .dropdown-header').addClass('dropdown-selected');
    });
});
