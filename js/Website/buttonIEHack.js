function buttonIEHack(wrapper="",form="") {
    var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
    if (isIE) {

        if(wrapper==""){
            $(".cta-primary, .cta-secondary, .cta-tertiary").each(
                function() {
                    var id = $(this).attr('id');
                    if (!(id)) {
                        var time = new Date();
                        $(this).attr('id', 'id_' +time.getTime());
                    }
                    $('<style type="text/css">#' + id + ':hover:before, #' + $(this).attr('id') + ':hover:after{width: ' + parseInt($(this).outerWidth() + 20) + 'px!important; height: ' + parseInt($(this).outerHeight() + 20) + 'px!important;}</style>').appendTo('head');
                }
            )
        }else{
            var element=wrapper.find(".cta-primary, .cta-secondary, .cta-tertiary");
            var id=element.attr("id");
            $('<style type="text/css">#' + id + ':hover:before, #' + element.attr('id') + ':hover:after{width: ' + parseInt(element.outerWidth() + 20) + 'px!important; height: ' + parseInt(element.outerHeight() + 20) + 'px!important;}</style>').appendTo('head');
        }
    }
}
waitForLoad(".hs-cta-wrapper", ".cta_button ", function(wrapper, form){buttonIEHack("","")});
waitForLoad(".widget-type-form,.widget-type-blog_content","form", function(wrapper, form){buttonIEHack("","")});
waitForLoad(".hs_cos_wrapper_type_form, .hs_cos_wrapper_type_blog_comments, .hs_cos_wrapper_type_blog_subscribe","form", function(wrapper, form){
         var inputSubmit = form.find('input[type=submit]');
        var date = new Date();
        $(this).attr('id', date.getTime());
        inputSubmit.after('<button class="cta_button cta-tertiary" '+' id="id_' + date.getTime() + '">' + inputSubmit.val() + '</button>');
        var react=inputSubmit.attr("data-reactid");
        inputSubmit.next().attr("data-reactid",react);
        inputSubmit.remove();

        buttonIEHack(wrapper,form);
});


