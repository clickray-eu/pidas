    // scroll top
    function scrollTop() {
        $("html, body").animate({
            scrollTop: $(".resources-item-wrapper ").position().top - 0
        }, 1000);
    }

    // pagination 
    function pagination() {

        // all content number 
        var totalContent = $(".resources-item-wrapper > span > div.after-search").length;

        

        var getNumberOfPage = '';

        if( $('.item-per-page-settings').length != 0 ){

            getNumberOfPage = $('.item-per-page-settings span').text();//4;

        }else{
            getNumberOfPage = 6;
        }

        
        var posPerPage = parseInt(getNumberOfPage); 



        var totalPage = Math.round(totalContent / posPerPage);

        // generte page and pagination k
        if (posPerPage < totalContent) {


            $(".resources-item-wrapper > span > div.after-search:gt(" + (posPerPage - 1) + ")").hide();

            for (i = 0; i <= (totalPage - 1); i++) {
                console.log(totalPage);
                $(".pagination ul").append("<li><a href='javascript:void(0)'>" + (i + 1) + "</a></li>");
            }

        }


        // first page added active class
        $(".pagination li:first").addClass("active");


        // click function
        $(".pagination li").on("click", function() {

            $(".resources-item-wrapper").css('opacity', '0');

            var index = $(this).index() + 1;
            var gt = posPerPage * index;

            $(".pagination li").removeClass("active");
            $(this).addClass("active");

            setTimeout(function() {

                $(".resources-item-wrapper > span > div.after-search").hide();

                for (i = gt - posPerPage; i < gt; i++) {

                    $(".resources-item-wrapper > span > div.after-search:eq(" + i + ")").show();
                }

            }, 300)


            setTimeout(function() {
                $(".resources-item-wrapper").css('opacity', '1');
                scrollTop();
            }, 300)


        });
    }
    // custom search
    function customSearch() {


        var searchContent = '';

        $('#search-input').keyup(function(e) {

            var searchedword = e.target.value.toLowerCase();
            var books = $('.resources-single-item');


            // CLEAR PAGINATION 
            $('.pagination ul > li').remove()
            $(".no-results").remove();


            books.each(function() {

                searchContent = $(this).find('h3 a').text().toLowerCase() + $(this).find('p span').text().toLowerCase();

                if (searchContent.indexOf(searchedword) != -1) {
                    $(this).parent().css('display', 'block');
                    $(this).parent().addClass('after-search');
                } else {
                    $(this).parent().css('display', 'none');
                    $(this).parent().removeClass('after-search');
                }
                searchContent = '';


            })

            if ($(".resources-item-wrapper > span > div.after-search").length == 0) {
                $(".resources-item-wrapper  ").parent().append('<h2 class="no-results" >No Results...</h2>');
            }


            pagination();


        });

    }
    // sort by Category
    function sortbyCategory() {


        $(".cat-menu li a").on("click", function(e) {
            e.preventDefault();


            $(".resources-item-wrapper").css('opacity', '0');


            var catName = $(this).text().toLowerCase();
            var item = '';
            // console.log(catName);

            $('.pagination ul > li').remove()
            $(".no-results").remove();

            setTimeout(function() {

                $('.resources-item-wrapper > span > div').each(function() {

                    item = $(this).find('.resources-single-item').attr('data-category').toLowerCase();

                    $(this).removeClass('show-by-cat').removeClass('show-by-cat');
                    $(this).css('display', 'none');




                    if (catName === item) {
                        // console.log(this);
                        $(this).css('display', 'block');
                        $(this).addClass('after-search');
                    } else if (catName === "all") {
                        $(this).css('display', 'block');
                        $(this).addClass('after-search');
                    } else {
                        $(this).css('display', 'none');
                        $(this).removeClass('after-search');
                    }




                })




                pagination();

                $(".resources-item-wrapper").css('opacity', '1');
                scrollTop();
            }, 300)


        });

    }



    $(document).ready(function() {
        customSearch();
        // first load all elments is after search 
        $(".resources-item-wrapper > span > div").addClass('after-search');
        pagination();
        sortbyCategory();

    })






