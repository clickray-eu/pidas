$(document).ready(function () {
    new WOW().init();
    setTimeout(function(){
        headerMenuFlyout();
    },500);
    headerSearchInit();
    menuMobileInit();
    displayVideoAsPopup("a.va-video-play-btn", "href");

    $('.print-page').click(function(){
        window.print();
    })

});
function headerMenuFlyout(){
    if($("header.header--main .hs-menu-flow-horizontal > ul").length>0){

    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top",-20);
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top",77);

        $.each($("header.header--main .hs-menu-flow-horizontal > ul li.hs-menu-depth-2"),function(i,e){
            $(e).parent().css("max-width",$(e).parents("ul:first").css("width"));
            if($(e).find(">ul").length>0){
                //$(e).find(">ul").css("max-width",$(e).parent().css("width"));
                $.each($(e).find("li.hs-menu-depth-3"),function(j,el){

                    $(el).parent().css("max-width",$(el).parent().css("width"));
                });
            }
        });     
        $("header.header--main .hs-menu-flow-horizontal > ul > li").addClass("right"); 

    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top","");
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top","");
    var tooBig=false;
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li").mouseover(function(){       
       if(tooBig){           
            if(!$(this).hasClass("hover")){
                $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li").removeClass("hover");
                $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top","");
            }
            $(this).find("ul").offset({top:$(this).parent().offset().top})
            $(this).addClass("hover");
            }else{

            $(this).find("ul").css("top",-20);                      
            if(!$(this).index(0)) {
                $(this).find("ul").css("top",0);                      
            }
            $(this).removeClass("hover");
}
    });
    
    $(window).resize(function(){
    if(parseInt($(".header.header--main .menu").data("mobile-active"))>=window.innerWidth){
        $(".header.header--main").addClass("mobile");
        return;
    }else{
        $(".header.header--main").removeClass("mobile");
    }
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top",-20);
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top",77);       
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("height","auto");
    tooBig=false;
    $.each($("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul"),function(i,e){
        if($(e).offset().top+$(e).height()>=(parseInt($(window).scrollTop())+parseInt(window.innerHeight))){
            tooBig=true;
        }
    });
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top","");
    $("header.header--main .hs-menu-flow-horizontal > ul > li > ul").css("top","");
    $.each($("header.header--main .hs-menu-flow-horizontal > ul > li > ul > li"),function(i,e){
            $($(e).find("ul")).each(function(j,list){
                if(($(e).offset().left+parseInt($(e).css("width"))+parseInt($(list).css("width")))>window.innerWidth-25){
                    $(list).addClass("left").removeClass("right");
                    $(e).addClass("left-arrow").removeClass("right-arrow");
                }else{
                    $(list).addClass("right").removeClass("left");
                    $(e).addClass("right-arrow").removeClass("left-arrow");
                }
                if(tooBig){
                    if($(list).parents("ul:first").height()>$(list).height())
                        $(list).css("height",$(list).parents("ul:first").css("height"));
                }else{
                    $(list).css("height","auto");                    
                    $(list).css("opacity","");
                }
            });
            
      });
    });

$(window).resize();

}
}
function menuMobileInit(){
    $('header.header--main .menu .hs-menu-wrapper>ul').slicknav({
            prependTo:'header.header--main>.container>.span12>.row-fluid-wrapper:first-of-type>.row-fluid',
            label:"",
            duration: 500,
            init:function(){
                $("header.header--main .slicknav_menu .slicknav_nav>li:first").before("<li class='search'></li>");
                $("header.header--main .slicknav_menu .slicknav_nav>li:last").after("<li class='follow-us'></li>");
                var search=$("header.header--main .google-search-module-popup .google-search-module-form").clone(true);
                $("header.header--main .slicknav_menu .slicknav_nav li.search").append(search);
                $("header.header--main .slicknav_menu .slicknav_nav li.follow-us").append($("header.header--main .follow-us-module").html());
            },
    });
    $('.header .slicknav_menu .slicknav_btn').on('click', function() {
        $('header.header--main .slicknav_menu .slicknav_nav').toggleClass('open');
    })
}
function headerSearchInit(){
    $("header.header--main .google-search-module-btn").click(function(){
        if(!$("header.header--main .google-search-module-popup").hasClass("show-popup"))
            $("header.header--main .google-search-module-popup").addClass("show-popup")
        else
            $("header.header--main .google-search-module-popup").removeClass("show-popup")

    });
    $("header.header--main .google-search-module-popup-close").click(function(){
        if($("header.header--main .google-search-module-popup").hasClass("show-popup"))
            $("header.header--main .google-search-module-popup").removeClass("show-popup");

    });
    $("header.header--main .google-search-module-popup").click(function(e){
        e.stopPropagation();
        if($("header.header--main .google-search-module-popup").hasClass("show-popup"))
            $("header.header--main .google-search-module-popup").removeClass("show-popup");
    });
    $("header.header--main .google-search-module-form,header.header--main .google-search-module-icon,header.header--main .google-search-module-label").click(function(e){
        e.stopPropagation();
    });
    $("header.header--main .google-search-module-icon").click(function(){
        if($("header.header--main .google-search-module-popup form a").length>0)
            $("header.header--main .google-search-module-popup form a").click();
        else
            $("header.header--main #google-custom-search").submit();
    });
}

function displayVideoAsPopup(selector,attr){
    $(selector).not('.no-modal').each(function(i,e){
        var videoSrc=$(e).attr(attr);
        if($(e)[0].localName=="a"){
            $(e).attr("href","javascript:void(0)");    
        }
            $(e).magnificPopup({
            items: {
                type: 'inline',
                src: '<video controls preload="auto" width="100%" height="100%" src="'+videoSrc+'"></video>',
            },
            callbacks: {
                open: function() {
                  $('html').css('margin-right', 0);
                     $(this.content)[0].play()
        
                },
                close: function() {
                     $(this.content)[0].load();
        
                }
            }
            });
        
    });
}

function findMiddleSlide(slideClass) {
    var slidesNum, initialSlide;
    slidesNum = $(slideClass).length
    if (slidesNum > 2) {
        if (slidesNum % 2 == 0) {
            initialSlide = Math.floor(slidesNum/2) - 1;
        } else {
            initialSlide = Math.floor(slidesNum/2)
        }
    } else {
        initialSlide = 0;
    }
    return initialSlide;
}